
provider "aws" {
  region     = "us-east-1"
  assume_role {
  role_arn = "arn:aws:iam::xxxxxxxxxx:role/VPC_EC2_fullaccess"
  sts_regional_endpoints = "regional"
  access_key = "xxxxxxxxxxxxxxxxxxx"
  secret_key = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}

#Create a demo VPC
resource "aws_vpc" "demo" {
  cidr_block       = "192.168.0.0/16"

}
#Create a Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.demo.id
  }

#Public Subnet

resource "aws_subnet" "scientec-public-subnet" {
  vpc_id     = aws_vpc.demo.id
  cidr_block = "192.168.1.0/24"
  availability_zone ="us-east-1a"

  tags = {
    Name = "first-public-subnet"
  }
}

#Private Subnet
resource "aws_subnet" "scientec-private-subnet" {
  vpc_id     = aws_vpc.demo.id
  cidr_block = "192.168.2.0/24"
  availability_zone ="us-east-1a"

  tags = {
    Name = "first-private-subnet"
  }
}


#3Create Private Route Table
resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.scientec-vpc.id

  route {
      cidr_block = "192.168.0.0/16"
      vpc_id = aws_vpc.scientec-vpc.id
    }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.example.id
  }

#4 Create public route table 

resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.scientec-vpc.id

  route {
      cidr_block = "192.168.0.0/16"
      vpc_id = aws_vpc.scientec-vpc.id
    }

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.gw.id
    }
  

# Nat Gateway
resource "aws_nat_gateway" "example" {
  subnet_id     = aws_subnet.scientec-public-subnet.id
  depends_on = [aws_internet_gateway.gw]
}

#Network Load Balancer 
resource "aws_elb" "scienteclb" {
  name               = "scientec-elb"
  availability_zones = ["us-east-1a"]

  access_logs {
    bucket        = "scientecbuc"
    bucket_prefix = "scientec"
    interval      = 60
  }

  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 8000
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "arn:aws:iam::123456789012:server-certificate/certName"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8000/"
    interval            = 30
  }

  instances                   = [aws_instance.scienteclb.id]
  cross_zone_load_balancing   = false
  connection_draining         = false
}

#Security Group 
resource "aws_security_group" "allow_scientec" {
  name              = "security_group_for_scientec"
  description       = "Allow inbound traffic "
  vpc_id            = aws_vpc.scientec-vpc.id
  

  ingress {
    description     = "Http traffic from VPC"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks     = ["118.189.0.0/16"]
  }

    ingress {
    description     = "Http traffic from VPC"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks     = ["116.206.0.0/16"]
  }
    ingress {
    description     = "Http traffic from VPC"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks     = ["223.25.0.0/16"]
  }

  ingress {
    description     = "SSH traffic from anywhere"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

}

#Associate private subset with Route Table 
resource "aws_route_table_association" "private_subnet" {
  subnet_id      = aws_subnet.scientec-private-subnet.id
  route_table_id = aws_route_table.private-route-table.id
}

#Associate public subset with Route Table
resource "aws_route_table_association" "public_subnet" {
  subnet_id      = aws_subnet.scientec-public-subnet.id
  route_table_id = aws_route_table.public-route-table.id
}

#Create an EC2 Instance 
resource "aws_instance" "scientec_docker_instance" {
  ami               = "ami-070650c005cce4203"
  instance_type     = "t3.micro"
  availability_zone = "-1a"
  key_name          = "scientec-key"
  subnet_id         = "aws_subnet.scientec-private-subnet.id"

}
  user_data = <<-EOF
              #!/bin/bash
              sudo apt update -y
              apt-get install -y apt-transport-https ca-certificates curl software-properties-common
              curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
              add-apt-repository \
              "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
              $(lsb_release -cs) \
              stable"
              apt-get update
              apt-get install -y docker-ce
              usermod -aG docker ubuntu
              curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
              chmod +x /usr/local/bin/docker-compose
              docker pull nginx:1.18.0  
              EOF
  tags = {
    Name = "docker-application"
  }
     